//------------------------ Express ----------------------//


const express    = require('express');
const bodyParser = require('body-parser');
const db         = require('./private/client.js');
//-------------------
	const taskData = db.task;//next removal
//-------------------

const app        = express();

//---------------------------------------------------------

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//---------------------------------------------------------


app.get('/', function(req, res) {
	res.end('Connection is successfully!');
})






//----------------  Работа с моделью user  ----------------


/*
	поиск.. 
		->        // просто get запрос..
		<-        // отправит все найденные значения
		<- (Err)  // отправит текст..
	добавление..
		->        // post запрос с заголовком..
		               	name:  'Vasya'
		<-        // отправит текст 
		<- (Err)  // отправит текст
	обновление..
		->        // put запрос с заголовками..
		               	_id:  '59a839ede921330bd44a52fc'
		               	name:  'Vasya'
		<-        // отправит текст 
		<- (Err)  // отправит текст
	удаление..
		->        // delete запрос с заголовком..
		               	_id:  '59a839ede921330bd44a52fc'
		<-        // отправит текст 
		<- (Err)  // отправит текст
		*/


		app.route('/user')
		.get(function (req, res) {
			db.getAllUser().then( result => {
				if( result === false ){
					res.status.send('List of users is not defined')
				} else {
					res.status(200).send(result);
				}
			})
		})
		.post(function (req, res) {
			db.addUser(req.headers.name).then( result => {
				if(result === true){
					res.status(200).send('Operation was successfully completed');
				} else {
					res.status(400).send('Enter a new name-task and description in the headers')
				}
			})
		})
		.put(function(req,res) {
			db.updateUser(req.headers._id, req.headers.name).then(result => {
				if(result === true) {
					res.status(200).send('Operation was successfully completed');
				} else {
					res.status(400).send('Something went wrong');
				}
			})
		})
		.delete(function(req, res) {
			if(req.headers._id === undefined) {
				res.status(400).send('Specify in the title of the user\'s id');
			} else {
				db.deleteUser(req.headers._id).then(result => {
					if(result === true){
						res.status(200).send('Operation was successfully completed');
					} else {
						res.status(400).send('Uset with this ID is not defined');
					}
				})
			}
		});





//---------------------------------------------------------
//----------------  Работа с моделью task  ----------------


/*
	поиск.. 
		->        // get запрос с заголовком..
									search: 'любое значение..'
		<-        // отправит все найденные значения или пустой массив
		<- (Err)  // отправит текст..
	добавление..
		->        // post запрос с заголовком..
		               	task:  'Homework'
		              и с телом..
		              	{"description": "to throw out the trash"}
		<-        // отправит текст 
		<- (Err)  // отправит текст
	обновление..
		->        // put запрос с заголовком..
		               	_id:  '59a839ede921330bd44a52fc'
		<-        // отправит текст 
	удаление..
		->        // delete запрос с заголовком..
		               	_id:  '59a839ede921330bd44a52fc'
		<-        // отправит текст 
		*/
		app.route('/task')
		.get(function (req, res) {// добавить обработик на пустой массив..
			db.getTask(req.headers.search).then(result => {
				if(result === false){
					res.status(404).send('List is not defined');
				} else {
					res.status(200).send(result);
				}
			})
		})
		.post(function (req, res) {
			if(req.headers.task === undefined){
				res.status(400).send('Enter a new name - task the headers');
			} else {
				db.addTask(req.headers.task, req.body.description).then(result => {
					if(result === true){
						res.status(200).send('Operation was successfully completed');
					} else {
						res.status(400).send('Enter a new name - task the headers');
					}
				})
			}
		})
		.put(function(req, res) {
			db.updateTask(req.headers._id, req.body).then(result => {
				if(result === true){
					res.status(200).send('Operation was successfully completed');
				} else {
					res.status(400).send('The task has not been updated')
				}
			})
		})
		.delete(function(req, res) {
			db.deleteTask(req.headers._id).then(result => {
				if(result === false){
					res.status(400).send('User is not defined');
				} else {
					res.status(200).send('Operation was successfully completed');
				}
			})
		});


/*
Функция обновляет статус задачи 
принимает id - задачи и её новый status в заголовк
если введен id или status не верно или же вообще не введены
вернет текст..

иначе обновит статус и вернет текст "cool"..
*/
app.post('/task/status', function(req, res) {
	db.updateStatusTask(req.headers._id,req.headers.status).then(result => {
		if(result === true){
			res.status(200).send('Status was successfully updated');
		} else {
			res.status(400).send('Status is not updated');
		}
	})
});
/*
функция принимает id задачи, а следом id добавляемого пользователя
в случае ошибки ничего не выполнит, просто вернет текст..
иначе обновит делегацию на пользователя..
*/
app.post('/task/delegate', function(req, res) {
	db.delegateTask(req.headers.task_id, req.headers.user_id).then(result => {
		if(result === true){
			res.status(200).send('Task was successfully delegated');
		} else {
			res.status(400).send('Task is not delegated');
		}
	})
});




//---------------------------------------------------------

module.exports   = app;

//---------------------------------------------------------