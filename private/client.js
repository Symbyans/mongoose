//--------------------------- Mongoose ------------------------//


const mongoose			 = require('mongoose');
//подключаем нашу библиотеку к проекту..
mongoose.connect('mongodb://localhost:27017/ex_db');
//конектимся к базе данной
//---------------------------------------------------------------

const db 						 = mongoose.connection;

// как-то добавить авторизацию... чтоб данная переменная--v 
// хранила ссылку на данные зарегистривовшегося пользователя
let client          = {
	_v: 0,
	_id: 0000,
	name: 'Vasya'
};
//создаем переменную хранящую результат о соединении

db.on('error', console.error.bind(console, 'connection error: '));
//при не удачной попытке соединения вывести к cmd error
//в случае успеза выполнить след. ф-ию
db.once('open', () => {console.log('----- Data base is connected! -----') });


//---------------------------------------------------------------


//Создаем схему и объекты..
const Schema = mongoose.Schema;

const UserScheme    = new Schema({
	name: {
		type: String,
		default: 'NoName'
	}
});

const taskSheme    = new Schema({
	name: String,
	description: {
		type: String,
		default: 'Description is not defined...'
	},
	status: {
		type: Boolean,
		default: true
	}, 
	user: {
		type: Object,
		default: client
	}
});


//Создаем объекты/модели по этой схеме..
const user     = mongoose.model('user', UserScheme);
const task    = mongoose.model('task', taskSheme);
//---------------------------------------------------------------





// --- work with users --- 
function getAllUser() {
	return new Promise( (res, rej) => {
		user.find({}, (err, docs) => {
			if(err){
				rej(false);
			} else {
				res(docs);
			}
		})
	})
};
function addUser(name) {
	return new Promise( (res, rej) => {
		user.create( { name: name }, (err, data) => {
			if(err){ 
				rej(false);
			} else {
				res(true);
			}
		});
	})
};
function updateUser(ID, newName) {
	return new Promise(( res, rej ) => {
		user.where({_id: ID}).update({$set: {name: newName}}).exec();
		res(true);
	})
};
function deleteUser(ID) {
	return new Promise((res, rej) => {
		user.findOne({_id: ID}, (err, data) => {
			if(err || data === null){
				res(false);
			} else {
				data.remove();
				res(true);
			}
		})
	})
};
//---------------------------------------------------------------



// --- work with task --- 
function getTask(keyword) {
	return new Promise ((res, rej) => {
		if(keyword === undefined){
			task.find({}, (err, docs) => {
				if(err){
					rej(false);
				} else {
					res(docs);
				}
			})
		} else {
			task.find({
				$or: [
				{ name : {$regex: keyword} },
				{ description : {$regex: keyword} }
				]
			},
			(err, docs) => {
				if(err){ 
					res(false);
				} else {
					res(docs);
				}
			})
		}
	})
};
function addTask(name, description) {
	return new Promise((res, rej) =>{
		task.create( { 
			name: name, 
			description: description || undefined
		}, (err, data) => {
			if(err){ 
				res(false);
			} else{
				res(true);
			}
		})
	})
};
function updateTask(ID, newData) {
	return new Promise((res, rej) => {
		task.findOne({_id: ID}, (err, oldData) => {
			if(err || ((newData.name === undefined)&&(newData.description === undefined))) {
				res(false);
			}	else {

				let name = newData.name || oldData.name;
				let description = newData.description || oldData.description;

				oldData.update({$set:{description: description, name: name}}).exec();
				res(true);
			}
		})
	})
};
function deleteTask(ID) {
	return new Promise((res, rej) => {
		task.findOne({ _id: ID}, ( err, docs ) => {
			if( err || docs === null){
				res(false);
			} else {
				docs.remove();
				res(true);
			}
		})
	})
};
//--------- open or delete task -----------
function updateStatusTask(ID, newStatus) {
	return new Promise((res, rej) => {
		task.findOne({ _id: ID}, ( err, docs ) => {
			if( err || docs === null){
				res(false);
			} else if(newStatus === 'true'){
				docs.update({$set:{status: true}}).exec();
				res(true);
			} else if(newStatus === 'false'){
				docs.update({$set:{status: false}}).exec();
				res(true);		
			} else {
				res(false);
			}
		})
	})
}
//-----delegate of tesk on user.. or update user------
function delegateTask(taskID, userID) {
	return new Promise((res, rej) => {
		task.findOne({_id: taskID}, ( err, dataTask ) => {
			if( err || dataTask === null){
				res(false);
			} else {
				user.findOne({_id: userID},( err, dataUser )=> {
					if(err || dataUser === null){
						res(false);
					} else {
						dataTask.update({ $set: {user: dataUser} }).exec();
						res(true);		
					}
				})
			}
		})
	})
}






//---------------------------------------------------------------


//Экспортируем наши ф-ии
module.exports = {
	addUser, getAllUser, updateUser, deleteUser,
	addTask, getTask,    updateTask, deleteTask,

	updateStatusTask,
	delegateTask
}