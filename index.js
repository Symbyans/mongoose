//------------------------- Server ----------------------//


const http    = require('http');
const app     = require('./express.js');

const server  = http.createServer(app);

let port      = process.env.PORT || 3000;
//--------------------------------------------------------


server.on('listening', () => {
	console.log('We are live on localhost: ' + port);
});

server.listen(port);